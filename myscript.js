function validateForm(){
	var name = document.forms["parcelcalculator"]["name"].value;
    var length = document.forms["parcelcalculator"]["length"].value;
    var width = document.forms["parcelcalculator"]["width"].value;
    var height = document.forms["parcelcalculator"]["height"].value;
    var mode = document.forms["parcelcalculator"]["mode"].value;
    var type = document.forms["parcelcalculator"]["type"].value;

    var weight = (length * width * height) / 5000;

    if(type == "Domestic" && mode == "Surface"){
    	if(weight < 2)
    		cost = 7.00;
    	else 
    		cost = 7.00 + ((weight-2) * 1.50);
    }
    else if(type == "Domestic" && mode == "Air"){
    	if(weight < 2)
    		cost = 10.00;
    	else 
    		cost = 10.00 + ((weight-2) * 3.00);
    }
    else if(type == "International" && mode == "Surface"){
    	if(weight < 2)
    		cost = 20.00;
    	else 
    		cost = 20.00 + ((weight-2) * 3.00);
    }
    else if(type == "International" && mode == "Air"){
    	if(weight < 2)
    		cost = 50.00;
    	else 
    		cost = 50.00 + ((weight-2) * 5.00);
    }

    alert("Parcel Volumetric and Cost Calculator \nCustomer name: " +name+ "\nLength: " +length+ " cm \nWidth: " +width+ " cm \nHeight: " +height+ " cm \nWeight: " +weight+ " kg \nMode: " +mode+ "\nType: " +type+ "\nDelivery Cost: RM " +cost)

}

function confirmreset(){
	alert("The inputs will be reset");
}

function uppercase() {
  var x = document.getElementById("name");
  x.value = x.value.toUpperCase();
}